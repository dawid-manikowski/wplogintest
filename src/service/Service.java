package service;

import main.Main;
import pageObjects.*;
import org.testng.annotations.Test;
import steps.waits.Methods;

public class Service extends Main {

    @Test
    public static void logIn() throws InterruptedException {

        WordpressFilesOperations_Component wordpressFilesOperations = new WordpressFilesOperations_Component();
        wordpressFilesOperations.step1();
        wordpressFilesOperations.step2();
        wordpressFilesOperations.step3();

        WordpressInvoice_Component wordpressInvoice = new WordpressInvoice_Component();
        wordpressInvoice.step1();
        wordpressInvoice.step2();

        WordpressMailing_Component wordpressMailing = new WordpressMailing_Component();
        wordpressMailing.step1();
        wordpressMailing.step2();
        wordpressMailing.step3();

        WordpressReadParameters_Component wordpressReadParameters = new WordpressReadParameters_Component();
        wordpressReadParameters.step1();
        wordpressReadParameters.step2();

        Methods.openPage("http://51.83.43.62/wordpress/wp-admin/");

        WordpressMainPage_Component wordpressMain = new WordpressMainPage_Component();
        wordpressMain.logIn("JanAutomatTrzeci", wordpressReadParameters.getPassword());
        wordpressMain.checkIfLoggingSuccess();

        Thread.sleep(5000); //for demo purpose, to show successful to clients
        
    }
}